﻿using System;
using Android.Widget;
using Android.App;
using System.Collections.Generic;
using Android.Views;
using POC.EntityModel;

namespace AndroidDemoPoc
{
	public class CustomerAdaptorClass:BaseAdapter
	{
		Activity context;
		List<CustomerModel> lstCategory;
		internal event Action<CustomerModel> CustomerNameClicked; 
		TextView lblCategory;
		public CustomerAdaptorClass (Activity c,List<CustomerModel> lstCategoryArg )
		{
			context = c;
			lstCategory = lstCategoryArg;
		}
		public override int Count {
			get { return lstCategory.Count; }
		}

		public override Java.Lang.Object GetItem (int position)
		{
			return null;
		}

		public override long GetItemId (int position)
		{
			return 0;
		}
		// create a new ImageView for each item referenced by the Adapter
		public override View GetView (int position, View convertView, ViewGroup parent)
		{
			ViewHolderCategoryClass vh;
			View view = convertView;
			if ( view == null )
			{
				view = context.LayoutInflater.Inflate ( Resource.Layout.ListCustomAdaptorLayout, parent , false ); 
				vh = new ViewHolderCategoryClass ();
				vh.initialize ( view ); 
				view.Tag = vh; 
			}

			vh = ( ViewHolderCategoryClass ) view.Tag;
			vh.viewClicked = () =>
			{
				if ( CustomerNameClicked != null )
				{
					CustomerNameClicked ( lstCategory [position] );
				}
			};
			lblCategory = view.FindViewById<TextView> ( Resource.Id.lblName );
			lblCategory.Text = lstCategory [position].FirstName + " " + lstCategory [position].LastName;
			//lblCategory.PaintFlags = Android.Graphics.PaintFlags.UnderlineText;

			return view;
		}  
	}

}




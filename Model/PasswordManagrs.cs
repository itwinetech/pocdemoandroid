﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
namespace AndroidDemoPoc
{
	public static class PasswordManagrs
	{
		#region Private Methods
		private static string GetSaltString()
		{
			const int SALT_SIZE = 24;
			string saltString = CommonClassFn.RandomString(SALT_SIZE);
			return saltString;
		}

		private static string GetPasswordHashAndSalt(string message)
		{
			// Let us use SHA256 algorithm to 
			// generate the hash from this salted password
			SHA256 sha = new SHA256Managed();
			byte[] dataBytes = CommonClassFn.GetBytes(message);
			byte[] resultBytes = sha.ComputeHash(dataBytes);
			// return the hash string to the caller
			return Convert.ToBase64String(resultBytes);// Utilities.GetString(resultBytes);
		}
		#endregion

		#region Public Methods

		public static string GeneratePasswordHash(string plainTextPassword, out string salt)
		{
			salt = GetSaltString();

			string finalString = plainTextPassword + salt;

			return GetPasswordHashAndSalt(finalString);
		}

		public static bool IsPasswordMatching(string password, string salt, byte[] hash)
		{
			string finalString = password + salt;
			return Convert.ToBase64String(hash) == GetPasswordHashAndSalt(finalString);
		}
		#endregion
	}
}


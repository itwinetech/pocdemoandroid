﻿using System;
using System.Text.RegularExpressions;

namespace AndroidDemoPoc
{
	public static class Constants
	{
		//General
		public const string strAppName="Demo Poc";
		public const string strNoInternet="No internet connection. Make sure your network connectivity in active and try again.";
		public const string strMandatoryFields="You must fill in all of the fields.";
		public const string strLoginFaild="Login Faild";
		public const string strEmailIdError="Invalid mail id";
		public const string strPasswordNotMatch="Password does not match the confirm password";
		public const string strMobileNOVAlidation="Please provide 10 digit MobileNo ";

		public const string strRegistrationFailed="Customer Details Registred UnSuccessfully";
		public const string strRegistrationSuccess="Customer Details Registerd Successfully";
		public const string strSaveCustomerDataSuccess="Customer Details Saved Successfully";
		public const string strSaveCustomerDataFailed="Customer Details Saved UnSuccessfully";

		internal static readonly string strDeletedCustomerSuccessMsg="Customer deleted successfully";
		internal static readonly string strDeletedCustomerUnSuccessMsg="Could not able to delete customer!";
		internal static readonly string strDeleteConfirmation="Are you sure want to delete customer!";
		public const string strCustomerUpdateSuccessMsg ="Customer Details Updated";
		public const string strCustomerUpdateUnSuccessMsg="Unsuccessfull Updated";
		public const string strSelectDataBase="Please Select Database.";
	}
}
﻿using System;
using System.Text.RegularExpressions;
using Android.App;
using Android.Content;
using Android.Net;
using System.IO;

namespace AndroidDemoPoc
{
	static	public class CommonClass
	{
		const string strEmailPattern=@"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
			@"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$";
		const string strMobilePattern = @"^[1-9]{1}[0-9]{9}$";

		//Login Activity
		static	internal bool FnFieldsValidation(string strMail,string strPassword)
		{
			bool isSuccess=false;
			if ( string.IsNullOrEmpty ( strMail ) && string.IsNullOrEmpty ( strPassword ) )
			{
				isSuccess=true;
			}
			return isSuccess;
		}
		//sign Up Activity Validation
		static	internal bool FnSignUpFieldsEmptyValidation(string strFirstName,string strLastName,string strEmail,string strPassword,string strRetypePAssword)
		{
			bool isSuccess=false;
			if (string.IsNullOrEmpty ( strFirstName )&& string.IsNullOrEmpty ( strLastName )&& string.IsNullOrEmpty ( strEmail ) && string.IsNullOrEmpty ( strPassword )&& string.IsNullOrEmpty ( strRetypePAssword ) )
			{
				isSuccess=true;
			}
			return isSuccess;
		}
		// UpDate Validation
		static	internal bool FnUpdateFieldsEmptyValidation(string strFirstName,string strLastName,string strEmail,string strDOB,string strMobile,string strAddress)
		{
			bool isSuccess=false;
			if (string.IsNullOrEmpty ( strFirstName )&& string.IsNullOrEmpty ( strLastName )&& string.IsNullOrEmpty ( strEmail ) && string.IsNullOrEmpty ( strDOB )&& string.IsNullOrEmpty ( strMobile ) && string.IsNullOrEmpty ( strAddress ))
			{
				isSuccess=true;
			}
			return isSuccess;
		}
		static	internal bool FnEmailValidation(string strMail)
		{
			Regex rex = new Regex (strEmailPattern);
			return rex.IsMatch (strMail);
		}

		static	internal bool FnMobileNoValidation(string strMobileNo)
		{
			Regex rex = new Regex (strMobilePattern);
			return rex.IsMatch (strMobileNo);
		}

		//single button Alertmessage
		static	internal void FnAlertMssg(string strTitle,string strMessage,Context context)
		{
			AlertDialog alert = new AlertDialog.Builder (context).Create ();
			{

				alert.SetCancelable (false);
				alert.SetTitle (strTitle);
				alert.SetMessage (strMessage);
				alert.SetButton ("ok", delegate(object sender , DialogClickEventArgs e )
				{
					if ( e.Which == -1 )
					{
						alert.Dismiss ();
						alert = null;
					}
				});
				alert.Show ();
			}

		}
		//2 button Alertmessage




		static	internal Boolean FnIsConnected(Context context)
		{
			try
			{
				ConnectivityManager connectionManager = (ConnectivityManager)context.GetSystemService (Android.Content.Context.ConnectivityService);
				NetworkInfo networkInfo = connectionManager.ActiveNetworkInfo; 
				if (networkInfo != null && networkInfo.IsConnected) 
				{
					return true;
				}
			}
			catch(Exception e)
			{
				Console.WriteLine (e.Message);
				return true;
			}
			return false;
		} 

		static	internal string FnGetExternalStoragepath()
		{
			return Android.OS.Environment.ExternalStorageDirectory.AbsolutePath;
		}
		static	internal bool FnIsMediaMounted()
		{
			string strState = Android.OS.Environment.ExternalStorageState;
			return strState.Equals ( Android.OS.Environment.MediaMounted );
		}
		static	internal bool FnCreateFolderInSDCard(string strMainFolder,string strSubFolderName,out string strCreatedFolderPath)
		{
			bool isSuccess = false;
			string strOutParameter=string.Empty;
			bool isMediaMounted = FnIsMediaMounted ();
			if ( isMediaMounted )
			{
				try
				{
					string strRootDirectory	= FnGetExternalStoragepath();
					string strProjectFolder = System.IO.Path.Combine ( strRootDirectory , strMainFolder );
					strOutParameter=strProjectFolder;
					if ( !Directory.Exists ( strProjectFolder ) )
					{
						Directory.CreateDirectory ( strProjectFolder );
					}
					if(!string.IsNullOrEmpty(strSubFolderName))
					{
						string strSubFolderToCreate = System.IO.Path.Combine ( strProjectFolder , strSubFolderName );
						strOutParameter=strSubFolderToCreate;
						if ( !Directory.Exists ( strSubFolderToCreate ) )
						{
							Directory.CreateDirectory ( strSubFolderToCreate );
						}
					}
					isSuccess=true;
				}
				catch
				{
					isSuccess= false;
				}
			}
			strCreatedFolderPath = strOutParameter;
			return isSuccess;
		}
	}
}


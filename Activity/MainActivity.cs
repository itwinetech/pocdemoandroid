using System;
using Android.OS;
using Android.Support.V4.Widget;
using Android.Support.V7.App;
using Android.Support.V7.Widget;
using Android.Support.Design.Widget;
using RestSharp;
using System.Collections.Generic;
using POC.EntityModel;
using Android.Widget;
using System.Net;
using Android.Content;
using Android.Views;
using Android.App;
using System.Threading.Tasks;

namespace AndroidDemoPoc
{
	[Activity ( Icon = "@mipmap/icon")]
	public class MainActivity : AppCompatActivity
	{
		//Update Initilization
		EditText txtFirstName;
		EditText txtLastName;
		EditText txtEmailId;
		EditText txtMobileNo;
		EditText txtDOB;
		TextView lblTitleHeader;
		EditText txtAddress;
		Button btnUpdate;
		Button btnDelete;
		CustomerModel objCustomerModel;

		int CustomerId=0;
		DrawerLayout drawerLayout;
		ScrollView scrollView;
		RestClient _client;
		ImageView imgPlusAdd;
		ListView listCustomer;
		DateTime date;
		ProgressDialog progress;
		bool isButtonSelected=false;
		const int Date_Id = 0;
		RelativeLayout rlyUpdateLayout;
		ImageView imgProfilePic;
		NavigationView navigationView;

		protected override void OnCreate(Bundle bundle)
		{			
			base.OnCreate(bundle);
			SetContentView(Resource.Layout.Main);

			FnInitilization ();
			FnClickEvent ();

		}
		public override void OnBackPressed ()
		{
			//base.OnBackPressed ();
			rlyUpdateLayout.Visibility = Android.Views.ViewStates.Gone;
			listCustomer.Visibility = Android.Views.ViewStates.Visible;
		}

		#region "Datepicker"
		private void UpdateDisplay ()
		{
			txtDOB.Text = date.ToString ();
		}

		protected Dialog CreateDialog (int id)
		{
			switch ( id )
			{
				case Date_Id:
					return new DatePickerDialog (this, OnDateSet, date.Year, date.Month-1, date.Day); 
			}
			return null;
		}
		void OnDateSet (object sender, DatePickerDialog.DateSetEventArgs e)
		{
			this.date = e.Date;
			UpdateDisplay ();

		}
		#endregion

		#region "Initilization"
		void FnInitilization()
		{
			drawerLayout = FindViewById<DrawerLayout>(Resource.Id.drawer_layout);
			listCustomer = FindViewById<ListView> (Resource.Id.listView1);
			navigationView = FindViewById<NavigationView>(Resource.Id.nav_view);
			imgProfilePic = FindViewById<ImageView> ( Resource.Id.imgProfilePicM );
			scrollView=FindViewById<ScrollView> ( Resource.Id.scrollView );
			//Update Initilization
			rlyUpdateLayout = FindViewById<RelativeLayout> ( Resource.Id.relUpdateLayoutM );
			txtFirstName = FindViewById<EditText> (Resource.Id.txtFirstNameUCL);
			txtLastName = FindViewById<EditText> (Resource.Id.txtLastNameUCL);
			txtEmailId = FindViewById<EditText> (Resource.Id.txtEmailUCL);
			txtMobileNo = FindViewById<EditText> (Resource.Id.txtMobileNoUCL);
			txtDOB = FindViewById<EditText> (Resource.Id.txtDobUCL);
			txtAddress = FindViewById<EditText> (Resource.Id.txtAddressUCL);

			btnUpdate=FindViewById<Button> (Resource.Id.btnUpdateUCL);
			//btnUpdate.TransformationMethod ( null );

			lblTitleHeader = FindViewById<TextView> ( Resource.Id.lblTitleUCL );
			btnDelete=FindViewById<Button> (Resource.Id.btnDeleteUCL);
			objCustomerModel = objCustomerModel ?? new CustomerModel ();
			// Init toolbar
			var toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);
			 imgPlusAdd = FindViewById<ImageView> ( Resource.Id.imgPlusSymbolM );
			SetSupportActionBar(toolbar);	

			// Create ActionBarDrawerToggle button and add it to the toolbar
			var drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, Resource.String.open_drawer, Resource.String.close_drawer);
			drawerLayout.SetDrawerListener(drawerToggle);
			drawerToggle.SyncState();
			_client = new RestClient (FirstActivity.strApiUri);
			date = DateTime.Today;
			txtDOB.Focusable = false;

			btnUpdate.SetAllCaps (false);
			btnDelete.SetAllCaps (false);
		} 
		#endregion

		#region "ClickEvent"
		void FnClickEvent()
		{
			navigationView.NavigationItemSelected += FnNavigationItemSelected;
			txtDOB.Click += delegate
			{       
				
				CreateDialog (Date_Id).Show ();
			};
			btnUpdate.Click += delegate(object sender , EventArgs e )
			{
				FnUpdateCustomerDetails();
			};

			btnDelete.Click += delegate(object sender , EventArgs e )
			{
				FnAlertMsgTwoInput(Constants.strAppName,Constants.strDeleteConfirmation,"Ok","Cancel",this);
				//Delete(CustomerId,objCustomerModel);
			};
			imgPlusAdd.Click += delegate(object sender , EventArgs e )
			{
				isButtonSelected=false;
				if(isButtonSelected)
				{
					 btnUpdate.Text="Update";

				}
				else
				{
					btnUpdate.Text="Save";
					lblTitleHeader.Text="Create Customer Details";

				}
				listCustomer.Visibility = Android.Views.ViewStates.Gone;
				rlyUpdateLayout.Visibility = Android.Views.ViewStates.Visible;
				FnClearTextField();
			};
		}
		#endregion

		#region "UpdateCustomerDetails"
		async	void FnUpdateCustomerDetails()
		{
			bool isMandatoryFields = CommonClass.FnUpdateFieldsEmptyValidation ( txtFirstName.Text , txtLastName.Text , txtEmailId.Text , txtDOB.Text , txtMobileNo.Text , txtAddress.Text );
			if ( isMandatoryFields )
			{
				CommonClass.FnAlertMssg ( Constants.strAppName , Constants.strMandatoryFields , this );
			}
			else
			{
				bool isMailValidation = CommonClass.FnEmailValidation ( txtEmailId.Text );
				if ( !isMailValidation )
				{
					CommonClass.FnAlertMssg ( Constants.strAppName , Constants.strEmailIdError , this );
				}
				else
				{
					bool isMobileNoValidation = CommonClass.FnMobileNoValidation ( txtMobileNo.Text );
					if ( !isMobileNoValidation )
					{
						CommonClass.FnAlertMssg ( Constants.strAppName , Constants.strMobileNOVAlidation , this );
					}
					else
					{
						bool isConnected = CommonClass.FnIsConnected ( this );
						if ( !isConnected )
						{
							CommonClass.FnAlertMssg ( Constants.strAppName , Constants.strNoInternet , this );
						}
						else
						{
							
							if ( isButtonSelected )
							{
								objCustomerModel.FirstName = txtFirstName.Text;
								objCustomerModel.LastName = txtLastName.Text;
								objCustomerModel.EmailId = txtEmailId.Text;
								objCustomerModel.DOB =Convert.ToDateTime( txtDOB.Text);
								objCustomerModel.Mobile = txtMobileNo.Text;
								objCustomerModel.Address = txtAddress.Text;
								progress = ProgressDialog.Show (this, "", "Please Wait...");
								bool isCorrect = await Task.Run (( ) =>	Edit ( objCustomerModel ));
								if ( isCorrect && !string.IsNullOrEmpty (LoginActivity.strToken ) )
								{
									FnDismissProgressDialog ();
									Toast.MakeText(this,Constants.strCustomerUpdateSuccessMsg,ToastLength.Short).Show();
									FnClearTextField();
									rlyUpdateLayout.Visibility = ViewStates.Gone;
									listCustomer.Visibility = ViewStates.Visible;
									imgPlusAdd.Visibility=ViewStates.Visible;
									FnGetCustomerList();
								}
								else
								{
									CommonClass.FnAlertMssg ( Constants.strAppName , Constants.strCustomerUpdateUnSuccessMsg, this );
								}
							}
							else
							{
								progress = ProgressDialog.Show (this, "", "Please Wait...");
								bool isCorrect = await Task.Run (( ) =>	FnSave ( objCustomerModel ));
								if ( isCorrect && !string.IsNullOrEmpty (LoginActivity.strToken ) )
								{
									FnDismissProgressDialog ();
									Toast.MakeText(this,Constants.strSaveCustomerDataSuccess,ToastLength.Short).Show();
									FnClearTextField();
									rlyUpdateLayout.Visibility = ViewStates.Gone;
									listCustomer.Visibility = ViewStates.Visible;
									imgPlusAdd.Visibility=ViewStates.Visible;
									FnGetCustomerList ();
								}
								else
								{
									CommonClass.FnAlertMssg (Constants.strAppName, Constants.strSaveCustomerDataSuccess, this);
								}
							}
						}
					}
				}
			}
		}
		#endregion

		void FnDismissProgressDialog()
		{
			if ( progress != null )
			{
				progress.Dismiss ();
				progress = null;
			}
		}

		#region "NavigationItemSelected(Drawer)"
		void FnNavigationItemSelected(object sender, NavigationView.NavigationItemSelectedEventArgs e)
		{
			switch (e.MenuItem.ItemId)
			{
				case (Resource.Id.nav_Dadhboard):
					listCustomer.Visibility = Android.Views.ViewStates.Gone;
					imgPlusAdd.Visibility = Android.Views.ViewStates.Gone;
					scrollView.Visibility = Android.Views.ViewStates.Visible;
					rlyUpdateLayout.Visibility = ViewStates.Gone;

					break;
				case (Resource.Id.nav_Customers):
					scrollView.Visibility = Android.Views.ViewStates.Gone;
					listCustomer.Visibility = Android.Views.ViewStates.Visible;
					imgPlusAdd.Visibility = Android.Views.ViewStates.Visible;
					rlyUpdateLayout.Visibility = ViewStates.Gone;
					FnGetCustomerList ();
					break;  
				case (Resource.Id.nav_LogOut):
					LogOut ();
					break;
			}
			// Close drawer
			drawerLayout.CloseDrawers();
		}
		#endregion

		#region "LogOut"
		internal void LogOut()
		{
			var request = new RestRequest ( "api/SignOut" , Method.GET ) { RequestFormat = DataFormat.Json };
			request.AddHeader("Token", LoginActivity.strToken);
			request.AddHeader("UserId", LoginActivity.strCustomerId);				 

			var response = _client.Execute ( request );
			if ( response.StatusCode == HttpStatusCode.OK )
			{
				StartActivity ( typeof ( FirstActivity ) );
				Finish ();
			}
		}
		#endregion

		#region "GetCustomerList"
		public void FnGetCustomerList()
		{
			var request = new RestRequest("api/Customer", Method.GET) { RequestFormat = DataFormat.Json };
			request.AddHeader("Token",LoginActivity.strToken);

			var response = _client.Execute<List<CustomerModel>>(request);

			if (response.Data != null) 
			{
				//response.Data is list of data of type CustomerModel
				CustomerAdaptorClass objCustomerAdaptorClass=new CustomerAdaptorClass (this,response.Data);
				listCustomer.Adapter = objCustomerAdaptorClass;
				objCustomerAdaptorClass.CustomerNameClicked+=CustomerRowTapped;
			} 

		}
		#endregion

		#region "CustomerRowTapped(list row)"
		void CustomerRowTapped(CustomerModel objCustomerModel)
		{
			isButtonSelected = true;
			if(isButtonSelected)
			{
				btnUpdate.Text="Update";
			}

			else
			{
				btnUpdate.Text="Save";
				lblTitleHeader.Text="Create Customer Details";
			}
			if ( !objCustomerModel.DOB.HasValue)// == DateTime.MinValue )
			{
				objCustomerModel.DOB = DateTime.MinValue;
			}
			 CustomerId=	objCustomerModel.CustomerId;
			FnFillCustomerData (objCustomerModel);
			listCustomer.Visibility = ViewStates.Gone;
			rlyUpdateLayout.Visibility =ViewStates.Visible;
		}
		#endregion

		#region "FillCustomerData"
		void FnFillCustomerData(CustomerModel customerModel)
		{
			txtFirstName.Text = customerModel.FirstName;
			txtLastName.Text = customerModel.LastName; 
			txtEmailId.Text = customerModel.EmailId;
			txtMobileNo.Text = customerModel.Mobile;
			txtDOB.Text = customerModel.DOB.ToString();
			txtAddress.Text = customerModel.Address;
			objCustomerModel = customerModel;
		}
		#endregion

		#region "Edit Customer Dettails"
		public bool Edit(CustomerModel customer)
		{
			try
			{
				var request = new RestRequest("api/Customer/Update", Method.POST) { RequestFormat = DataFormat.Json };
				request.AddHeader("Token",LoginActivity.strToken);
				request.AddJsonBody(customer); 

				var response = _client.Execute(request); 

				if (response.StatusCode == HttpStatusCode.OK)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch
			{
				return false;
			}
			return false;
		}
		#endregion

		#region "ClearTextField"
		void FnClearTextField()
		{
			txtFirstName.Text = string.Empty;
			txtLastName.Text = string.Empty;
			txtEmailId.Text = string.Empty;
			txtDOB.Text = string.Empty;
			txtMobileNo.Text = string.Empty;
			txtAddress.Text = string.Empty;
		}
		#endregion

		#region "AlertMsgTwoInput(2 button)"
		internal  void FnAlertMsgTwoInput (string strTitle,string strMsg,string strOk,string strCancel,Context context)
		{
			Android.App.AlertDialog alertMsg = new Android.App.AlertDialog.Builder (context).Create ();
			alertMsg.SetCancelable (false);
			alertMsg.SetTitle (strTitle);
			alertMsg.SetMessage (strMsg);
			alertMsg.SetButton2(strOk, delegate (object sender, DialogClickEventArgs e) 
			{
				if (e.Which  == -2) 
				{
					FnDelete(CustomerId,objCustomerModel);
				}
			});
			alertMsg.SetButton(strCancel,delegate (object sender, DialogClickEventArgs e) 
			{
				if (e.Which  == -1) 
				{
					alertMsg.Dismiss ();
					alertMsg=null;
				}
			});
			alertMsg.Show ();
		} 
		#endregion

		#region "Delete customer details"
		public bool FnDelete(long id, CustomerModel customer)
		{
			try
			{
				var request = new RestRequest("api/Customer/Delete/{id}", Method.POST);
				request.AddHeader("Token", LoginActivity.strToken);
				request.AddParameter("id", id, ParameterType.UrlSegment);

				var response = _client.Execute(request);

				if (response.StatusCode == HttpStatusCode.OK)
				{
					//(response.ErrorMessage);
					Toast.MakeText(this,Constants.strDeletedCustomerSuccessMsg,ToastLength.Short).Show();
					FnClearTextField();
					rlyUpdateLayout.Visibility = Android.Views.ViewStates.Gone;
					FnGetCustomerList();
					listCustomer.Visibility = Android.Views.ViewStates.Visible;
				}
				else
				{
					CommonClass.FnAlertMssg (Constants.strAppName, Constants.strDeletedCustomerUnSuccessMsg, this);
				}
			}
			catch
			{

			}
			return true;
		}
		#endregion

		#region "Save customer record(new records)"
		internal bool FnSave(CustomerModel customer)
		{
			try 
			{
				customer.FirstName =	txtFirstName.Text;
				customer.LastName = txtLastName.Text;
				customer.EmailId =	txtEmailId.Text;
				customer.Mobile = txtMobileNo.Text; 
				customer.DOB = Convert.ToDateTime( txtDOB.Text);
				customer.Address = txtAddress.Text;

				string strAlertMsg = string.Empty;
				var request = new RestRequest ("api/Customer/Create", Method.POST) { RequestFormat = DataFormat.Json };
				request.AddHeader ("Token", LoginActivity.strToken);
				request.AddJsonBody (customer);
				var response = _client.Execute<CustomerModel> (request);

				if (response.StatusCode == HttpStatusCode.Created)
				{ 
//					strAlertMsg = Constants.strSaveCustomerDataSuccess;
//					FnGetCustomerList();
					return true;
				} 
				else
				{
					//strAlertMsg = Constants.strSaveCustomerDataFailed;
					return false;
				}
				//CommonClass.FnAlertMssg (Constants.strAppName, strAlertMsg, this);
			} 
			catch 
			{
				CommonClass.FnAlertMssg (Constants.strAppName, Constants.strSaveCustomerDataFailed, this);
			}
			return false;
		}
		#endregion
	}
}
﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace AndroidDemoPoc
{
	[Activity ( Label = "Demo Poc",MainLauncher = true  )]			
	public class FirstActivity : Activity
	{
		Button btnSign;
		Button btnRegister;
		RadioButton rdbMySql;
		RadioButton rdbOracle;
		RadioButton rdbMsSql;
		int rdButton=0;

		string strMYSql="http://202.83.22.139/pocwebapi";
		string strOracle="http://202.83.22.139/pocwebapioracle";
		string strMsSql="http://202.83.22.139/pocwebapimssql";

		internal static	string strApiUri;

		protected override void OnCreate ( Bundle savedInstanceState )
		{
			base.OnCreate ( savedInstanceState );
			SetContentView ( Resource.Layout.FirstLayout );
			FnInitilization ();
			FnClickEvent ();

		}

		#region "Initilization"
		void FnInitilization()
		{
			btnSign = FindViewById<Button> ( Resource.Id.btnSignFL );
			btnRegister = FindViewById<Button> ( Resource.Id.btnRegisterFL );
			rdbMySql = FindViewById<RadioButton> ( Resource.Id.rdbMySql );
			rdbOracle = FindViewById<RadioButton> ( Resource.Id.rdbOracle );
			rdbMsSql = FindViewById<RadioButton> ( Resource.Id.rdbMsSql );

		}
		#endregion

		#region "ClickEvent"
		void FnClickEvent()
		{
			btnSign.Click += delegate(object sender , EventArgs e )
			{
				if(rdButton==0)
				{
					CommonClass.FnAlertMssg(Constants.strAppName,Constants.strSelectDataBase,this);
				}
				else
				{
					if(rdButton==1)
					{
						strApiUri=strMYSql;
					}
					else if(rdButton==2)
					{
						strApiUri=strOracle;
					}
					else if(rdButton==3)
					{
						strApiUri=strMsSql;
					}
					StartActivity(typeof(LoginActivity));
				}

			};
			btnRegister.Click += delegate(object sender , EventArgs e )
			{
				if(rdButton==1)
				{
					strApiUri=strMYSql;
				}
				else if(rdButton==2)
				{
					strApiUri=strOracle;
				}
				else if(rdButton==3)
				{
					strApiUri=strMsSql;
				}
				StartActivity(typeof(SignUpActivity));
			};

			rdbMySql.Click += RadioButtonClicked;
			rdbOracle.Click += RadioButtonClicked;
			rdbMsSql.Click += RadioButtonClicked;
		}
		#endregion

		#region "RadioButtonClicked"
		void RadioButtonClicked(object sender,EventArgs e)
		{
			RadioButton rd = (RadioButton)sender;
			bool isChecked = rd.Checked;
			if(isChecked)
			{
				switch(rd.Id)
				{
					case Resource.Id.rdbMySql:
						rdbOracle.Checked = false;
						rdbMsSql.Checked = false;
						rdButton=1;
						break;
					case Resource.Id.rdbOracle:
						rdbMySql.Checked = false;
						rdbMsSql.Checked = false;
						rdButton=2;
						break;
					case Resource.Id.rdbMsSql:
						rdbMySql.Checked = false;
						rdbOracle.Checked = false;
						rdButton=3;
						break;
				}
			}
		}
		#endregion
	}
}


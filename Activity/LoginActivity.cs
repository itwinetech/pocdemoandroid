﻿
using System;
using System.Linq;
using Android.App;
using Android.OS;
using Android.Widget;
using System.Threading.Tasks;
using RestSharp;

namespace AndroidDemoPoc
{
	[Activity ( Label = "Demo Poc")]			
	public class LoginActivity : Activity
	{
		//Control Initilization
		Button btnSignIn;
		EditText txtEmail;
		EditText txtPassword;
		TextView lblForgotPassword;
		TextView lblRegisterNewMemberShip;
		CheckBox chkRememberMe;

		ProgressDialog progress;
		internal static string strToken="";
		internal static	string strCustomerId;
		RestClient _client;

		protected override void OnCreate ( Bundle savedInstanceState )
		{
			base.OnCreate ( savedInstanceState );
			SetContentView ( Resource.Layout.LoginLayout );
			FnInitilization ();
			FnClickEvent ();
		}
		#region "Initilization"
		void FnInitilization()
		{
			btnSignIn = FindViewById<Button> ( Resource.Id.btnSignInLL );
			txtEmail=FindViewById<EditText> ( Resource.Id.txtEmailLL);
			txtPassword=FindViewById<EditText> ( Resource.Id.txtPasswordLL );
			chkRememberMe = FindViewById<CheckBox> (Resource.Id.chkRemberMeLL);
			lblForgotPassword = FindViewById<TextView> ( Resource.Id.lblForgotPAsswordLL );
			lblRegisterNewMemberShip = FindViewById<TextView> ( Resource.Id.lblRegisterNewMemberShipLL );

			_client = new RestClient (FirstActivity.strApiUri);

		}
		#endregion

		#region "Click Event"
		void FnClickEvent()
		{
			btnSignIn.Click +=async delegate(object sender , EventArgs e )
			{
				bool isMandatoryFields = CommonClass.FnFieldsValidation ( txtEmail.Text , txtPassword.Text );
				if ( isMandatoryFields )
				{
					CommonClass.FnAlertMssg ( Constants.strAppName , Constants.strMandatoryFields , this );
				}
				else
				{
					bool isMailValidation = CommonClass.FnEmailValidation ( txtEmail.Text );
					if ( !isMailValidation )
					{
						CommonClass.FnAlertMssg ( Constants.strAppName , Constants.strEmailIdError , this );
					}
					else
					{
						bool isConnected = CommonClass.FnIsConnected ( this );
						if ( !isConnected )
						{
							CommonClass.FnAlertMssg ( Constants.strAppName , Constants.strNoInternet , this );
						}
						else
						{
							progress = ProgressDialog.Show (this, "", "Please Wait...");
							bool isCorrect = await Task.Run (( ) =>	SignIn ( txtEmail.Text , txtPassword.Text ));
							if ( isCorrect && !string.IsNullOrEmpty( strToken) )
							{
								txtEmail.Text=string.Empty;
								txtPassword.Text=string.Empty;
								StartActivity ( typeof ( MainActivity ) );
								Finish();
							}
							else
							{
								CommonClass.FnAlertMssg ( Constants.strAppName , Constants.strLoginFaild , this );
							}
						}
					}
				}
				if ( progress != null )
				{
					progress.Dismiss ();
					progress = null;
				}
			};
			lblForgotPassword.Click+=delegate(object sender , EventArgs e )
			{
				
			};
			lblRegisterNewMemberShip.Click+=delegate(object sender , EventArgs e )
			{
				StartActivity(typeof (SignUpActivity));
			};
		}
		#endregion

		#region "Sign In Function"
		 bool SignIn(string strEmailId,string strPassword)
		{  
			try
			{
				var request = new RestRequest ( "api/login" , Method.POST ) { RequestFormat = DataFormat.Json };
				request.AddParameter ( "Authorization" , string.Concat ( "Basic " ,CommonClassFn.EncryptData ( strEmailId + ":" + strPassword ) ) , ParameterType.HttpHeader );
				string strdataP =CommonClassFn.EncryptData ( strEmailId + ":" + strPassword );
				var response=_client.Execute(request);
				if ( !string.IsNullOrEmpty ( response.Content ) )
				{
					strToken = response.Headers.FirstOrDefault ( a => a.Name == "Token" ).Value.ToString ();
					strCustomerId=response.Headers.FirstOrDefault (a => a.Name == "UserId").Value.ToString();
					return true	;	
				}
										
			}
			catch(Exception e)
			{

				return false;
			}
			return false;
		}
		#endregion
	}
}


﻿using System;
using Android.App;
using Android.OS;
using Android.Widget;
using POC.EntityModel;
using RestSharp;
using System.Net;
using System.Threading.Tasks;

namespace AndroidDemoPoc
{
	[Activity ( Label = "SignUpActivity" )]			
	public class SignUpActivity : Activity
	{
		EditText txtFirstName;
		EditText txtLastName;
		EditText txtEmailId;
		EditText txtPassword;
		EditText txtRetypePassword;
		Button btnRegister;
		CheckBox chkTermsCondition;
		TextView lblAlerdyMember;

		bool isChecked=false;
		RestClient _client;
		ProgressDialog progress;
		UserRegistrationModel objUserRegistrationModel;

		protected override void OnCreate ( Bundle savedInstanceState )
		{
			base.OnCreate ( savedInstanceState );
			SetContentView ( Resource.Layout.SignUpLayout );
			FnInitilization ();
			FnClickEvent ();
		}

		#region "Initilization"
		void FnInitilization()
		{
			btnRegister = FindViewById<Button> (Resource.Id.btnRegisterSUL);
			txtFirstName = FindViewById<EditText> (Resource.Id.txtFirstNameSUL);
			txtLastName = FindViewById<EditText> (Resource.Id.txtLastNameSUL);
			txtEmailId = FindViewById<EditText> (Resource.Id.txtEmailSUL);
			txtPassword = FindViewById<EditText> (Resource.Id.txtPasswordSUL);
			txtRetypePassword = FindViewById<EditText> (Resource.Id.txtRetypePasswordSUL);
			chkTermsCondition = FindViewById<CheckBox> (Resource.Id.checkBoxSUL);
			lblAlerdyMember = FindViewById<TextView> ( Resource.Id.lblMemberShipSUL );
			_client = new RestClient (FirstActivity.strApiUri);
			objUserRegistrationModel = objUserRegistrationModel ?? new UserRegistrationModel ();
		}
		#endregion

		#region "ClickEvent"
		void FnClickEvent()
		{
			btnRegister.Click +=async delegate(object sender , EventArgs e )
			{
				bool isMandatoryFields = CommonClass.FnSignUpFieldsEmptyValidation ( txtFirstName.Text , txtLastName.Text , txtEmailId.Text , txtPassword.Text , txtRetypePassword.Text );
				if ( isMandatoryFields )
				{
					CommonClass.FnAlertMssg ( Constants.strAppName , Constants.strMandatoryFields , this );
				}
				else
				{
					bool isMailValidation = CommonClass.FnEmailValidation ( txtEmailId.Text );
					if ( !isMailValidation )
					{
						CommonClass.FnAlertMssg ( Constants.strAppName , Constants.strEmailIdError , this );
					}
					else
					{
						if ( !(txtPassword.Text == txtRetypePassword.Text ))
						{
							CommonClass.FnAlertMssg ( Constants.strAppName , Constants.strPasswordNotMatch , this );
						}
						else
						{
							bool isConnected = CommonClass.FnIsConnected ( this );
							if ( !isConnected )
							{
								CommonClass.FnAlertMssg ( Constants.strAppName , Constants.strNoInternet , this );
							}
							else
							{
								objUserRegistrationModel.FirstName = txtFirstName.Text;
								objUserRegistrationModel.LastName = txtLastName.Text;
								objUserRegistrationModel.EmailId = txtEmailId.Text;
								objUserRegistrationModel.Password = txtPassword.Text;
								objUserRegistrationModel.ConfirmPassword = txtRetypePassword.Text;
								objUserRegistrationModel.IAgreeToTerms = isChecked;

								progress = ProgressDialog.Show (this, "", "Please Wait...");
								bool isCorrect = await Task.Run (( ) =>FnRegistration ( objUserRegistrationModel ));
								if ( isCorrect && !string.IsNullOrEmpty(LoginActivity.strToken) )
								{
									FnDismissProgressDialog();
									Console.WriteLine ("registered");
									Toast.MakeText(this,Constants.strRegistrationSuccess,ToastLength.Short).Show();
									FnClearTextFields ();
									StartActivity ( typeof ( LoginActivity ) );
								}
								else
								{
									FnDismissProgressDialog();
									Console.WriteLine ("unsuccessful");
									CommonClass.FnAlertMssg ( Constants.strAppName , Constants.strRegistrationFailed , this );
								}

							}
						}
					}
				}
			};
			chkTermsCondition.CheckedChange+= delegate(object sender , CompoundButton.CheckedChangeEventArgs e )
			{
				isChecked=e.IsChecked;
			};
			lblAlerdyMember.Click += delegate(object sender , EventArgs e )
			{
				StartActivity(typeof(LoginActivity));
			};
		}
		#endregion

		void FnDismissProgressDialog()
		{
			if ( progress != null )
			{
				progress.Dismiss ();
				progress = null;
			}
		}


		#region "Registration Method"
		bool FnRegistration(UserRegistrationModel customer)
		{ 
			var request = new RestRequest("api/customer/Register", Method.POST) { RequestFormat = DataFormat.Json };
			request.AddJsonBody(customer);
			var response = _client.Execute(request);
			if (response.StatusCode == HttpStatusCode.Created)
			{
				return true;
			}
			else
			{ 
				return false;
			}
			return false;
		}
		#endregion

		#region "Clear TextFields"
		void FnClearTextFields ()
		{
			txtFirstName.Text = string.Empty;
			txtLastName.Text = string.Empty;
			txtEmailId.Text = string.Empty;
			txtPassword.Text = string.Empty;
			txtRetypePassword.Text = string.Empty;
		}
		#endregion
	}
}

